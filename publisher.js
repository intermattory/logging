/**
 * Implements the publisher pattern. Function creates the publisher object and trigger function.
 * @return {Promise<unknown>|({forgetAll: (function(): void), forget: (function(...[*]=): void), subscribe: subscribe, unsubscribe: unsubscribe, readonly promise: Promise<unknown>}|(function(...[*]): *[]))[]}
 * @constructor
 * @example
 * function MessageService() {
 *    const [messageReceived, publishMessageReceived, throwError] = Publisher();
 *    // ...
 *    service.onMessage = async data => {
 *       const jsonData = await JSON.parse(data);
 *       const message = await decodeMessage(jsonData.content);
 *       publishMessageReceived(message);
 *    }
 *    // ...
 *    return { messageReceived };
 * }
 *
 * function Conversation() {
 *     // ...
 *     messageService.connected.promise.then(() => {
 *        logger.info('Connected');
 *        messageService.messageReceived.subscribe(messageReceiveListener);
 *     });
 *     // ...
 *     messageService.disconnected.promise.then(reason => {
 *        logger.info('Disconnected', reason);
 *        messageService.messageReceived.unsubscribe(messageReceiveListener);
 *     });
 *     // ...
 * }
 *
 * @example
 * function MessageService() {
 *    const [messageReceived, publishMessageReceived, throwError] = Publisher();
 *    // ...
 *    service.onMessage = async data => {
 *      try {
 *          const jsonData = await JSON.parse(data);
 *          const message = await decodeMessage(jsonData.content);
 *          publishMessageReceived(message);
 *       } catch (error) {
 *          throwError(error);
 *       }
 *    }
 *    // ...
 *    return { messageReceived };
 * }
 */
export function Publisher() {
    const listenerList = [];
    const errorListenerList = [];
    const promiseCallbacks = new Map();
    const subscribe = (...listener) => {
        listenerList.push(...listener);
    };
    const unsubscribe = (...listeners) => {
        Array.from(listeners).forEach(listener => {
            const listenerIndex = listenerList.findIndex(l => l === listener);
            if (listenerIndex >= 0)
                listenerList.splice(listenerIndex, 1);
            const errorListenerIndex = errorListenerList.findIndex(l => l === listener);
            if (errorListenerIndex >= 0)
                errorListenerList.splice(errorListenerIndex, 1);
        });
    };
    const getPromise = () => {
        let callbacks;
        const promise = new Promise((resolve, reject) => callbacks = {resolve, reject});
        const listener = (...data) => {
            callbacks.resolve(...data);
            unsubscribe(listener);
        };
        try {
            subscribe(listener);
        } catch (error) {
            callbacks.reject(error);
            unsubscribe(listener);
        }
        promiseCallbacks.set(promise, callbacks);
        return promise;
    };
    const forget = (...promises) => Array.from(promises).forEach(promise => {
        unsubscribe(promiseCallbacks.get(promise));
        promiseCallbacks.delete(promise);
    });
    const forgetAll = () => forget(...Array.from(promiseCallbacks.keys()));
    const catchError = (...listeners) => {
        errorListenerList.push(...listeners);
    };
    const triggerErrorListeners = function () {
        const args = Array.from(arguments);
        [...errorListenerList].forEach(listener => listener(...args));
        Array.from(promiseCallbacks.values()).forEach(callbacks => callbacks.reject(...args));
    };

    return [{
        subscribe,
        unsubscribe,
        catch: catchError,
        forget,
        forgetAll,
        get promise() { return getPromise(); }
    },
        // TODO Should the object (O) which instantiates the Publisher have access to the results of listeners?
        //  - why not? The listeners are impure by design, O can dictate what they return.
        function () {
            const args = Array.from(arguments)
            return [...listenerList].map(listener => listener(...args));
        },
        triggerErrorListeners
    ];
}

/**
 * Creates a publisher which triggers its listeners when any of given publishers publishes something and unsubscribes.
 * The listeners will get an array of results ordered respectively to the order of publishers. A result is an object of
 * following structure: `{publisher, value}`
 * @param publishers
 * @return {Promise<unknown>|{subscribe: subscribe, unsubscribe: unsubscribe, handle: (function(...[*]=): Promise<void>), readonly promise: Promise<unknown>}}
 * @example
 * const [submitEvent, saveEvent, cancelEvent] = await Publisher.any(submit, save, cancel).promise;
 * // If submit publishes an event:
 * // [{publisher, value}, undefined, undefined]
 * @example
 * Publisher.any(submit, save, cancel)
 *    .handle(
 *       data => service.submit(data),
 *       data => service.save(data),
 *       () => confirmCancel(),
 *    )
 */
Publisher.any = (...publishers) => {
    const publishersArray = Array.from(publishers);
    let listenerList = [];
    const getPublisherListener = index => function () {
        const args = Array.from(arguments)
        const publishersMap = publishersArray.map(() => undefined);
        publishersMap[index] = {};
        publishersMap[index].value = args.length === 0 ? undefined : args.length === 1 ? args[0] : args;
        publishersMap[index].publisher = publishersArray[index];
        listenerList.map(listener => listener(publishersMap));
    }
    const publisherListeners = new Map(publishersArray.map((publisher, index) => [publisher, getPublisherListener(index)]));
    const subscribe = (...listeners) => {
        listenerList = [...listenerList, ...listeners];
        publishersArray.forEach(publisher => publisher.subscribe(publisherListeners.get(publisher)));
        return {
            unsubscribe: () => unsubscribe(...listeners)
        }
    };
    const unsubscribe = (...unsubscribed) => {
        listenerList = listenerList.filter(item => !unsubscribed.includes(item));
        publishersArray.forEach(publisher => publisher.unsubscribe(publisherListeners.get(publisher)));
    };
    const map = (...listeners) => {
        listenerList = [...listeners].slice(0, publishersArray.length);
        if (listenerList.length !== publishersArray.length)
            console.warn(`Number of listeners (${listenerList.length}) does not match the number of publishers (${publishersArray.length}). Excessive elements will be ignored.`)
        publishersArray.forEach((publisher, index) => publisher.subscribe(listenerList[index]));
        return {
            unmap: () => publishersArray.forEach((publisher, index) => publisher.unsubscribe(listenerList[index]))
        }
    }
    const getPromise = () => {
        let callbacks;
        const promise = new Promise((resolve, reject) => callbacks = {resolve, reject});
        const listener = function () {
            const args = Array.from(arguments);
            callbacks.resolve(...args);
            unsubscribe(listener);
        };
        try {
            subscribe(listener);
        } catch (error) {
            callbacks.reject(error);
            console.warn('Publisher.any.promise: Could not subscribe');
            unsubscribe(listener);
        }
        return promise;
    };
    /**
     * Maps handlers to the given publishers by index.
     * At most one handler will be called.
     * @param handlers
     * @return {Promise<void>}
     */
    const handle = (...handlers) => getPromise()
        .then(results => {
            const resultHandlers = [...handlers];
            results.forEach((result, index) => result && handlers.length > index && handlers[index](result.value))
        })
        .catch();
    return { subscribe, unsubscribe, handle, map
        , get promise() { return getPromise(); } };
};
Publisher.fromPromise = promise => {
    const [publisher, trigger, throwError] = Publisher();
    promise.then(trigger).catch(throwError);
    return publisher;
};