
import { LogLevel } from '../log-level.js'

describe('LogLevel', () => {
	it('should return \'ALL\' for level 0', () => {
		expect(LogLevel.getLevelName(0)).toEqual('ALL');
	});
	it('should return \'DEBUG\' for level 100', () => {
		expect(LogLevel.getLevelName(100)).toEqual('DEBUG');
	});
	it('should return \'INFO\' for level 200', () => {
		expect(LogLevel.getLevelName(200)).toEqual('INFO');
	});
	it('should return \'WARN\' for level 300', () => {
		expect(LogLevel.getLevelName(300)).toEqual('WARN');
	});
	it('should return \'ERROR\' for level 400', () => {
		expect(LogLevel.getLevelName(400)).toEqual('ERROR');
	});
	it('should return \'FATAL\' for level 1000', () => {
		expect(LogLevel.getLevelName(1000)).toEqual('FATAL');
	});
	it('should return \'237\' for level 237', () => {
		expect(LogLevel.getLevelName(237)).toEqual('237');
	});
});