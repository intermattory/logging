
import { Log } from '../log.js';
import { LogLevel } from '../log-level.js'

const log = Log()
const Logger = (source, category) => log.Logger(source, {category})

describe('Logger', () => {
	beforeAll(() => {
		log.start();
	})

	it('should have logNumber = 0', () => {
		function Test() {}
		Logger(Test);
		expect(log.logNumber).toEqual(0);
	});

	it('should throw a ReferenceError if source is undefined', () => {
		let Type;
		function createLogger() {
			Logger(Type);
		}
		expect(createLogger).toThrow(ReferenceError, 'The \'source\' argument is not defined');
	});

	it('should throw a ReferenceError if source is null', () => {
		let Type = null;
		function createLogger() {
			Logger(Type);
		}
		expect(createLogger).toThrow(ReferenceError, 'The \'source\' argument is not defined');
	});

	xit ('should handle string source', () => {
		let Type = 'Test';
		try {
			Logger(Type);
		} catch (e) {
			assert(false, 'Logger constructor threw an error when called with string source');
		}
		expect(true);
	});

	/*it('should not override logNumber descriptor', () => {
		let result = Reflect.defineProperty(Log, 'logNumber', {configurable: true, value: 0});
		expect(result).toEqual(false);
	});

	it('should not delete logNumber descriptor', () => {
		let result = Reflect.deleteProperty(Log, 'logNumber');
		expect(result).toEqual(false);
	});*/

	it('log method should dispatch a LogEvent with a correct log entry (level: LogLevel.ALL)', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.ALL);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.log(LogLevel.ALL, 'Test', 'message');
	});

	it('log method should dispatch a LogEvent with a correct log entry (level: 500)', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(500);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.log(500, 'Test', 'message');
	});

	it('log.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.WARN);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.log.withError(LogLevel.WARN, error, 'Test', 'message');
	});

	it('debug method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.DEBUG);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.debug('Test', 'message');
	});

	it('debug.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.DEBUG);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.debug.withError(error, 'Test', 'message');
	});

	it('info method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.INFO);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.info('Test', 'message');
	});

	it('info.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.INFO);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.info.withError(error, 'Test', 'message');
	});

	it('warn method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.WARN);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.warn('Test', 'message');
	});

	it('warn.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.WARN);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.warn.withError(error, 'Test', 'message');
	});

	it('error method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.ERROR);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.error('Test', 'message');
	});

	it('error.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.ERROR);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.error.withError(error, 'Test', 'message');
	});

	it('fatal method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(undefined);
			expect(entry.level).toEqual(LogLevel.FATAL);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.fatal('Test', 'message');
	});

	it('fatal.withError method should dispatch a LogEvent with a correct log entry', (done) => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');
		let logMessageHandler = entry => {
			expect(entry.category).toEqual(category);
			// Expecting a log entry to be created within 100ms:
			expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
			expect(entry.error).toEqual(error);
			expect(entry.level).toEqual(LogLevel.FATAL);
			expect(entry.logNumber).toEqual(lastLogNumber + 1);
			expect(entry.message).toEqual(['Test', 'message']);
			expect(entry.source).toEqual(source.name);
			done();
		};

		logger.onMessage.promise.then(logMessageHandler);
		timestamp = Date.now();
		logger.fatal.withError(error, 'Test', 'message');
	});

	it('log method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.log(250, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(250);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('log.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.log.withError(250, error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(250);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('debug method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.debug('Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(LogLevel.DEBUG);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('debug.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.debug.withError(error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(LogLevel.DEBUG);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('info method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.info('Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(LogLevel.INFO);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('info.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.info.withError(error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(LogLevel.INFO);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('warn method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.warn('Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(LogLevel.WARN);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('warn.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.warn.withError(error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 1);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(LogLevel.WARN);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('error method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.error('Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(LogLevel.ERROR);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('error.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.error.withError(error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(LogLevel.ERROR);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('fatal method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;

		timestamp = Date.now();
		let entry = logger.fatal('Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(undefined);
		expect(entry.level).toEqual(LogLevel.FATAL);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	});

	it('fatal.withError method should return a correct log entry', () => {
		let source = function Test() {};
		let category = 'testing';
		let logger = Logger(source, category);
		let lastLogNumber = log.logNumber;
		let timestamp;
		let error = new Error('Test error');

		timestamp = Date.now();
		let entry = logger.fatal.withError(error, 'Test', 'message');

		expect(entry.category).toEqual(category);
		// Expecting a log entry to be created within 100ms:
		expect(entry.date.valueOf() / 100).toBeCloseTo(timestamp / 100, 0);
		expect(entry.error).toEqual(error);
		expect(entry.level).toEqual(LogLevel.FATAL);
		expect(entry.logNumber).toEqual(lastLogNumber + 1);
		expect(entry.message).toEqual(['Test', 'message']);
		expect(entry.source).toEqual(source.name);
	})
});