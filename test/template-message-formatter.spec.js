
import { TemplateMessageFormatter } from '../template-message-formatter.js'
import { LogLevel } from '../log-level.js'

describe('TemplateMessageFormatter', () => {

	let warnSpy

	it('should correctly format message', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: LogLevel.WARN,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][11:05:43.000][category: testing] Test: Test message Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format message with invalid date', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: {},
			logNumber: 11,
			level: LogLevel.WARN,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][No time][category: testing] Test: Test message Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format message with undefined date', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: null,
			logNumber: 11,
			level: LogLevel.WARN,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][No time][category: testing] Test: Test message Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format message with valid date', () => {
		const templateFunction = (logEntry, formatted) => `#${formatted.logNumber} [${formatted.level}][${formatted.date} ${formatted.time}][${formatted.parameters}] ${formatted.source}: ${formatted.message}${logEntry.error ? ' ' + formatted.errorName + ' ' + formatted.stackTrace : ''}`
		const formatter = TemplateMessageFormatter(templateFunction)
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: LogLevel.WARN,
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][2017-01-01 11:05:43.000][category: testing] Test: Test message'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format parameters', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: LogLevel.WARN,
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing',
			system: 'TestSystem',
			application: 'TestApplication'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][11:05:43.000][category: testing, system: TestSystem, application: TestApplication] Test: Test message'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format given parameter', () => {
		const templateFunction = (logEntry, formatted) => `#${formatted.logNumber} [${formatted.level}][${formatted.time}][${formatted.parameter('system')}] ${formatted.source}: ${formatted.message}${logEntry.error ? ' ' + formatted.errorName + ' ' + formatted.stackTrace : ''}`
		const formatter = TemplateMessageFormatter(templateFunction)
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: LogLevel.WARN,
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing',
			system: 'TestSystem',
			application: 'TestApplication'
		}
		const result = formatter.format(entry)
		const expected = '#11 [WARN][11:05:43.000][system: TestSystem] Test: Test message'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format message with custom level', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with string message', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: new Error('Test error'),
			message: 'Test message as string',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message as string Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with different message type', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: new Error('Test error'),
			message: 1234567890,
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: 1234567890 Error: Test error'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with empty error', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: undefined,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with string error', () => {
		const templateFunction = (logEntry, formatted) => `#${formatted.logNumber} [${formatted.level}][${formatted.time}][${formatted.parameters}] ${formatted.source}: ${formatted.message}${logEntry.error ? ' ' + formatted.errorName + ' ' + formatted.stackTrace : ''}`
		const formatter = TemplateMessageFormatter(templateFunction)
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: 'Test error',
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message Test error [No stack trace]'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format error message', () => {
		const templateFunction = (logEntry, formatted) => `#${formatted.logNumber} [${formatted.level}][${formatted.time}][${formatted.parameters}] ${formatted.source}: ${formatted.message}${logEntry.error ? ' ' + formatted.errorMessage + ' ' + formatted.stackTrace : ''}`
		const formatter = TemplateMessageFormatter(templateFunction)
		let error
		try {
			throw new Error('Test error')
		} catch (e) {
			error = e
		}
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: error,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message Test error ' + error.stack
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with other error types', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: {msg: 'Test error'},
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message [Custom error]'
		expect(result[0].substring(0, expected.length)).toEqual(expected)
	})

	it('should correctly format entry with error stack trace', () => {
		const templateFunction = (logEntry, formatted) => `#${formatted.logNumber} [${formatted.level}][${formatted.time}][${formatted.parameters}] ${formatted.source}: ${formatted.message}${logEntry.error ? ' ' + formatted.errorName + ' ' + formatted.stackTrace : ''}`
		const formatter = TemplateMessageFormatter(templateFunction)
		let error

		try {
			throw new Error('Test error')
		} catch (e) {
			error = e
		}

		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message Error ' + error.stack
		expect(result[0]).toEqual(expected)
	})

	it('should correctly format entry without date', () => {
		const formatter = TemplateMessageFormatter()
		formatter.date = false
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: null,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message'
		expect(result[0]).toEqual(expected)
	})

	it('should correctly format entry without time', () => {
		const formatter = TemplateMessageFormatter()
		formatter.time = false
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: null,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message'
		expect(result[0]).toEqual(expected)
	})

	it('should correctly format entry without date nor time', () => {
		const formatter = TemplateMessageFormatter()
		formatter.date = false
		formatter.time = false
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: null,
			message: 'Test message',
			source: 'Test',
			category: 'testing'
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: testing] Test: Test message'
		expect(result[0]).toEqual(expected)
	})

	it('should correctly format entry without category', () => {
		const formatter = TemplateMessageFormatter()
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: null,
			message: 'Test message',
			source: 'Test',
			category: undefined
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: undefined] Test: Test message'
		expect(result[0]).toEqual(expected)
	})

	it('should return [No entry]', () => {
		const formatter = TemplateMessageFormatter()
		const result = formatter.format(undefined)
		const expected = '[No entry]'
		expect(result[0]).toEqual(expected)
	})

	it('should warn if templateFunction is not a function', () => {
		warnSpy = jest.spyOn(console, 'warn')

		const formatterArgument = 'Some {name} template'
		const formatter = TemplateMessageFormatter(formatterArgument)
		const entry = {
			date: new Date(2017, 0, 1, 11, 5, 43),
			logNumber: 11,
			level: 234,
			error: null,
			message: 'Test message',
			source: 'Test',
			category: undefined
		}
		const result = formatter.format(entry)
		const expected = '#11 [234][11:05:43.000][category: undefined] Test: Test message'
		expect(result[0]).toEqual(expected)
		expect(warnSpy).toHaveBeenCalledWith(`TemplateMessageFormatter expect a function as argument. Provided argument is of type ${typeof formatterArgument}. Using default template function.`)

		warnSpy.mockRestore()
	})
})