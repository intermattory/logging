import { Log } from '../log.js'
import { LogLevel } from '../log-level.js'
import { ArrayTarget, ConsoleTarget, TemplateMessageFormatter } from '../index.js'

describe('Log', () => {
	let logSpy
	let debugSpy
	let infoSpy
	let warnSpy
	let errorSpy

	beforeEach(() => {
		logSpy = jest.spyOn(console, 'log')
		debugSpy = jest.spyOn(console, 'debug')
		infoSpy = jest.spyOn(console, 'info')
		warnSpy = jest.spyOn(console, 'warn')
		errorSpy = jest.spyOn(console, 'error')
	})

	afterEach(() => {
		logSpy.mockRestore()
		debugSpy.mockRestore()
		infoSpy.mockRestore()
		warnSpy.mockRestore()
		errorSpy.mockRestore()
	})

	it('should filter out category "testing"', () => {
		const arrayTarget = ArrayTarget()
		const configuration = {
			logTargets: [{
				target: arrayTarget,
				filter: logEntry => logEntry.category !== 'testing'
			}]
		}
		const log = Log(configuration)
		const Logger = (source, category) => log.Logger(source, {category})
		log.start()
		let testingLogger = Logger('Test', 'testing')
		let communicationLogger = Logger('MessageService', 'communication')

		testingLogger.info('Test info')
		communicationLogger.warn('Communication message warning')
		testingLogger.error('Test error')
		communicationLogger.debug('Communication debug message')

		expect(arrayTarget.array[0].logNumber).toEqual(2)
		expect(arrayTarget.array[0].message).toEqual(['Communication message warning'])
		expect(arrayTarget.array[0].category).toEqual('communication')
		expect(arrayTarget.array[0].level).toEqual(LogLevel.WARN)
		expect(arrayTarget.array[0].source).toEqual('MessageService')

		expect(arrayTarget.array[1].logNumber).toEqual(4)
		expect(arrayTarget.array[1].message).toEqual(['Communication debug message'])
		expect(arrayTarget.array[1].category).toEqual('communication')
		expect(arrayTarget.array[1].level).toEqual(LogLevel.DEBUG)
		expect(arrayTarget.array[1].source).toEqual('MessageService')
	})

	it('should notify about illegal parameters', () => {
		const log = Log()
		const Logger = log.Logger
		log.start()
		let testingLogger = Logger('Test', {date: '2022-02-19', logNumber: 19, level: LogLevel.ERROR, error: new Error('Test error'), message: 'Should be ignored', source: 'IgnoreMe'})

		testingLogger.info('Test info')
		expect(warnSpy).toHaveBeenCalledWith(`Argument 'parameters' contains illegal parameters: date, logNumber, level, error, message, source. They will be ignored.`)
	})

	it('should use custom log number generator', () => {
		const formatter = TemplateMessageFormatter((logEntry, formattedLogEntry) => `#${formattedLogEntry.logNumber} [${formattedLogEntry.level}]${formattedLogEntry.parameters ? `[${formattedLogEntry.parameters}]` : ''} ${formattedLogEntry.source}: ${formattedLogEntry.message}${logEntry.error ? ' ' + formattedLogEntry.error: ''}`)
		const consoleTarget = ConsoleTarget(formatter)
		const configuration = {
			logNumberGenerator: (logNumber, date) => `Test${logNumber}-${date.toISOString().substring(0, 19)}`,
			logTargets: [{
				target: consoleTarget,
				level: LogLevel.ALL
			}]
		}
		const log = Log(configuration)
		log.start()
		let logger = log.Logger('Test')
		const date = (new Date()).toISOString().substring(0, 19)

		logger.info('Test info')
		logger.warn('Test warning')
		expect(infoSpy).toHaveBeenCalledWith(`#Test1-${date} [INFO] Test: Test info`)
		expect(warnSpy).toHaveBeenCalledWith(`#Test2-${date} [WARN] Test: Test warning`)
	})
})