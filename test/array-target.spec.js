
import { ArrayTarget } from '../array-target.js';
import { LogLevel } from '../log-level.js'

describe('ArrayTarget', () => {
	function getTestEntry(level) {
		return {
			date: new Date(),
			logNumber: 11,
			level,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: function Test() {
			},
			category: 'testing'
		};
	}

	it('should log the message to the console', () => {
		let entry = getTestEntry(250);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the debug message to the console', () => {
		let entry = getTestEntry(LogLevel.DEBUG);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the info message to the console', () => {
		let entry = getTestEntry(LogLevel.INFO);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the warn message to the console', () => {
		let entry = getTestEntry(LogLevel.WARN);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the error message to the console', () => {
		let entry = getTestEntry(LogLevel.ERROR);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the fatal error message to the console', () => {
		let entry = getTestEntry(LogLevel.FATAL);
		let target = ArrayTarget([]);
		target.log(entry);
		expect(target.array).toHaveLength(1);
		expect(target.array[0]).toEqual(entry);
	});

	it('should log the diagnostic information when invalid entry was provided', () => {
		let target = ArrayTarget([]);
		target.log();
		expect(target.array).toHaveLength(0);
	});

	it('should log the diagnostic information when array is undefined', () => {
		let entry = getTestEntry(LogLevel.FATAL);
		let target = ArrayTarget();
		target.log(entry);
		expect(true);
	});

	it('should log the diagnostic information when array is null', () => {
		let entry = getTestEntry(LogLevel.FATAL);
		let array = null;
		let target = ArrayTarget(array);
		target.log(entry);
		expect(true);
	});

	it('should log the diagnostic information when array property is not an array', () => {
		let entry = getTestEntry(LogLevel.FATAL);
		let array = 'array';
		let target = ArrayTarget(array);
		target.log(entry);
		expect(true);
	});
});
