
import { ConsoleTarget } from '../console-target.js';
import { LogLevel } from '../log-level.js'

describe('ConsoleTarget', () => {
	function getTestEntry(level) {
		return {
			date: new Date(),
			logNumber: 11,
			level,
			error: new Error('Test error'),
			message: ['Test', 'message'],
			source: 'Test',
			category: 'testing'
		};
	}

	let logSpy;
	let debugSpy;
	let infoSpy;
	let warnSpy;
	let errorSpy;

	beforeEach(() => {
		logSpy = jest.spyOn(console, 'log');
		debugSpy = jest.spyOn(console, 'debug');
		infoSpy = jest.spyOn(console, 'info');
		warnSpy = jest.spyOn(console, 'warn');
		errorSpy = jest.spyOn(console, 'error');
	});

	afterEach(() => {
		logSpy.mockRestore();
		debugSpy.mockRestore();
		infoSpy.mockRestore();
		warnSpy.mockRestore();
		errorSpy.mockRestore();
	});

	it('should log the message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(250));
		expect(logSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the debug message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(LogLevel.DEBUG));
		expect(debugSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the info message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(LogLevel.INFO));
		expect(infoSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the warn message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(LogLevel.WARN));
		expect(warnSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the error message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(LogLevel.ERROR));
		expect(errorSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the fatal error message to the console', () => {
		let target = ConsoleTarget();
		target.log(getTestEntry(LogLevel.FATAL));
		expect(errorSpy).toHaveBeenCalledTimes(1);
	});

	it('should log the diagnostic information when invalid entry was provided', () => {
		let target = ConsoleTarget();
		target.log();
		expect(warnSpy).toHaveBeenCalledWith('[ConsoleTarget] Invalid entry')
	});
});