import { Publisher } from './publisher.js';

export function ArrayTarget(initialArray = []) {
	const [publisher, publish] = Publisher();
	if (!(Object.prototype.toString.call(initialArray) === '[object Array]')) {
		console.warn('[ArrayTarget] initialArray argument must be an Array, using empty array');
		initialArray = [];
	}

	let logArray = [...initialArray];
	// window.log = logArray;

    const log = entry => {
		let ret = false;
		if (!entry) {
			console.warn('[ArrayTarget] Invalid entry');
			ret = true;
		}
		if (ret) return;

		logArray.push(entry);
		publish(entry);
	};

    return {
    	...publisher,
		log,
		get array() { return logArray }
	}
}