import { Logger } from './demo-logger.js';
import { TemplateMessageFormatter } from '../index.js';

const logger = Logger('index', 'demo', 'test', 'tag2');
window.logger = logger;
window.TemplateMessageFormatter = TemplateMessageFormatter;

logger.warn.withError(new Error('Test error'), 'Starting demo', {test: 'Test'}, 23);
