import { Log, LogLevel, ArrayTarget, ConsoleTarget, TemplateMessageFormatter } from '../index.js'

window.log = []

const templateFunction = (entry, formattedEntry) => `[${formattedEntry.time}][${formattedEntry.level}] ${formattedEntry.logNumber}.${formattedEntry.source}: ${formattedEntry.message}`
const templateMessageFormatter = TemplateMessageFormatter(templateFunction)
const consoleTarget = ConsoleTarget(templateMessageFormatter)

const arrayTarget = ArrayTarget()
arrayTarget.subscribe(entry => window.log.push(entry))

const configuration = {
	logTargets: [
		{
			target: consoleTarget,
			level: LogLevel.ALL,
		},
		{
			target: arrayTarget,
			level: LogLevel.ALL
		}
	]
}
const demoLogging = Log(configuration);
const Logger = (source, category, ...tags) => demoLogging.Logger(source, {category, tags});

demoLogging.start();

export { Logger }