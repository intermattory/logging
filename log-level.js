
export const LogLevel = {
	ALL: 0,
	DEBUG: 100,
	INFO: 200,
	WARN: 300,
	ERROR: 400,
	FATAL: 1000,
	getLevelName: level => {
		switch (level) {
			case 0: return 'ALL';
			case 100: return 'DEBUG';
			case 200: return 'INFO';
			case 300: return 'WARN';
			case 400: return 'ERROR';
			case 1000: return 'FATAL';
			default: return String(level);
		}
	}
};