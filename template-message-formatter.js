import { LogLevel } from './log-level.js'

function isDate(object) {
	return Object.prototype.toString.call(object) === '[object Date]';
}

function formatDate(date) {
	if (!date || !isDate(date)) return 'Invalid date';
	return `${date.getFullYear()}-${(date.getMonth() + 1).toString().padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`;
}

function formatTime(date) {
	if (!date || !isDate(date)) return 'No time';
	return `${date.getHours().toString().padStart(2, '0')}:${date.getMinutes().toString().padStart(2, '0')}:${date.getSeconds().toString().padStart(2, '0')}.${date.getMilliseconds().toString().padStart(3, '0')}`;
}

function formatLevel(level) {
	let levelFormatted = String(level);

	for (let levelName in LogLevel)
		if (Reflect.has(LogLevel, levelName) && LogLevel[levelName] === level)
			return levelName;

	return levelFormatted;
}

function formatMessage(message) {
	if (typeof message === 'string') return message;
	if (Array.isArray(message)) return message.join(' ');
	return String(message);
}

function formatErrorName(error) {
	if (!error) return '';
	return error.name || error.toString();
}

function formatErrorMessage(error) {
	if (!error) return '';
	if (typeof error === 'string') return error;
	return error.message || '[No error message]';
}

function formatStackTrace(error) {
	if (!error) return '';
	return error.stack || '[No stack trace]';
}

function formatParameter(name, value) {
	return `${name}: ${value}`;
}

function formatParameters(entry) {
	const parameters = {...entry};
	delete parameters.date;
	delete parameters.logNumber;
	delete parameters.level;
	delete parameters.error;
	delete parameters.message;
	delete parameters.source;
	return Object.entries(parameters)
		.map(([parameter, value]) => `${parameter}: ${value}`)
		.join(', ');
}

export const getFormattedLogEntry = logEntry => ({
	get logNumber() { return logEntry.logNumber },
	get date() { return formatDate(logEntry.date) },
	get time() { return formatTime(logEntry.date) },
	get level() { return formatLevel(logEntry.level) },
	get source() { return logEntry.source || '[Unknown source]' },
	get message() { return formatMessage(logEntry.message) },
	get errorName() { return formatErrorName(logEntry.error) },
	get errorMessage() { return formatErrorMessage(logEntry.error) },
	get stackTrace() { return formatStackTrace(logEntry.error) },
	get error() { return !logEntry.error ? '' :
		typeof logEntry.error === 'string' ? logEntry.error :
		logEntry.error.message ? logEntry.error.toString() : '[Custom error]' },
	get parameters() { return formatParameters(logEntry) },
	get parameter() { return parameter => formatParameter(parameter, logEntry[parameter]) },
})

/**
 * Sample output:
 * #23 [WARN][03:15:54.986][category: testing, tags: tag1,tag2] index: Test message Error: Test error
 * @param logEntry
 * @param formattedLogEntry
 * @return {`#${string} [${string}][${*}][${*}] ${string}: ${string}${string|string}`}
 * @constructor
 */
export const DEFAULT_MESSAGE_TEMPLATE = (logEntry, formattedLogEntry) => `#${formattedLogEntry.logNumber} [${formattedLogEntry.level}][${formattedLogEntry.time}]${formattedLogEntry.parameters ? `[${formattedLogEntry.parameters}]` : ''} ${formattedLogEntry.source}: ${formattedLogEntry.message}${logEntry.error ? ' ' + formattedLogEntry.error: ''}`
export function TemplateMessageFormatter(templateFunction = DEFAULT_MESSAGE_TEMPLATE) {
	if (typeof templateFunction !== 'function') {
		console.warn(`TemplateMessageFormatter expect a function as argument. Provided argument is of type ${typeof templateFunction}. Using default template function.`);
		templateFunction = DEFAULT_MESSAGE_TEMPLATE;
	}

	return {
		format: entry => entry ? [templateFunction(entry, getFormattedLogEntry(entry))] : ['[No entry]']
	}
}
TemplateMessageFormatter.DEFAULT_MESSAGE_TEMPLATE = DEFAULT_MESSAGE_TEMPLATE