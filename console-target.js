import { LogLevel } from './log-level.js'
import { TemplateMessageFormatter } from './template-message-formatter.js'

function getDefaultFormatter() {
	return TemplateMessageFormatter()
}

export function ConsoleTarget(messageFormatter = getDefaultFormatter()) {
	const log = entry => {
		if (!entry) {
			console.warn('[ConsoleTarget] Invalid entry');
			return;
		}
		switch (entry.level) {
			case LogLevel.DEBUG:
				console.debug(...messageFormatter.format(entry));
				break;
			case LogLevel.INFO:
				console.info(...messageFormatter.format(entry));
				break;
			case LogLevel.WARN:
				console.warn(...messageFormatter.format(entry));
				break;
			case LogLevel.ERROR:
			case LogLevel.FATAL:
				console.error(...messageFormatter.format(entry));
				break;
			default:
				console.log(...messageFormatter.format(entry));
		}
	}
	return { messageFormatter, log };
}

ConsoleTarget.getDefaultConfiguration = function () {
	return {
		target: new ConsoleTarget(),
		level: LogLevel.ALL
	}
};