import { Publisher } from './publisher.js'
import { ConsoleTarget } from './console-target.js'
import { LogLevel } from './log-level.js'

const DEFAULT_LOG_NUMBER_GENERATOR = (logNumber, date) => logNumber
export const DEFAULT_LOGGER_CONFIGURATION = {
	logNumberGenerator: DEFAULT_LOG_NUMBER_GENERATOR,
	logTargets: [
		{
			target: ConsoleTarget(),
			level: LogLevel.ALL
		}
	]
}

/**
 * Log() creates an instance of the logging mechanism for the entire application or part of it
 * (If your system consists of multiple modules or libraries you can use the same logging mechanism with different
 * configurations, adjusted to the module).
 *
 * @example
 * import { Log } from 'pathToLogging/logging.js'
 *
 * // Initialize logging for the entire application or module:
 * const log = Log()
 * export const Logger = log.Logger
 * log.start()
 *
 * @example
 * // logger.js
 * import { Log } from 'pathToLogging/logging.js'
 *
 * // Initialize logging for the entire application or module:
 * const log = Log()
 * const Logger = (source, category, ...tags) => log.Logger(source, {category, tags})
 * log.start()
 * export { Logger }
 *
 * // shopping-cart.js
 * const logger = Logger('ShoppingCart', 'business', 'cart', 'order', 'payments')
 * // ...
 * logger.warn.withError(fetchReviewsError, 'Item reviews could not be fetched')
 *
 * @example
 * import { Log } from 'pathToLogging/logging.js'
 *
 * // Initialize logging for the entire application or module:
 * const log = Log()
 * const Logger = (source, module, product) => log.Logger(source, {module, product})
 * logging.start()
 * export { Logger }
 *
 * // shopping-cart.js
 * const logger = Logger('ShoppingCart', 'OnlineShop', 'YourEnterpriseSoftware')
 * // ...
 * logger.warn.withError(fetchReviewsError, 'Item reviews could not be fetched')
 *
 * @return {{stop: stop, readonly configuration: *, start: ((function(*=): (boolean))|*), Logger: (function(*, *=): Readonly<{warn: (function(...[*]): {[p: string]: *}), debug: (function(...[*]): {[p: string]: *}), log: (function(*=, ...[*]=): {[p: string]: *}), onMessage: *, error: (function(...[*]): {[p: string]: *}), info: (function(...[*]): {[p: string]: *}), fatal: (function(...[*]): {[p: string]: *})}>)}|*}
 *
 */
export function Log(configuration = DEFAULT_LOGGER_CONFIGURATION) {
	const config = Object.freeze({...DEFAULT_LOGGER_CONFIGURATION, ...configuration})
	let status

	let logNumber = 0
	let customLogNumber

	function nextLogNumber() {
		return ++logNumber
	}

	/**
	 * Starts listening to the logger messages
	 * @param configuration
	 * @return {boolean}
	 */
	const start = () => {
		if (status === 'started') {
			console.warn('[Logging] Logging already started')
			return false
		}
		status = 'started'
		return true
	}
	const stop = () => {
		status = 'stopped'
		// TODO Unsubscribe publishers
	}

	const matchesConfiguration = (entry, configuration) => {
		if (entry.level < configuration.level) return false
		if (configuration.filter && !configuration.filter(entry)) return false
		return true
	}

	const handleMessage = entry => {
		if (status !== 'started') {
			console.warn(`Logging hasn't been started yet. To start it, call log.start(config).`)
			return
		}
		config.logTargets.forEach(configuration => {
			if (matchesConfiguration(entry, configuration))
				configuration.target.log(entry)
		})
	}

	const validateParameters = parameters => {
		if (!parameters) return
		const illegalParameters = []
		const validParameters = {...parameters}
		delete validParameters.date
		delete validParameters.logNumber
		delete validParameters.level
		delete validParameters.error
		delete validParameters.message
		delete validParameters.source
		if (parameters.date) illegalParameters.push('date')
		if (parameters.logNumber) illegalParameters.push('logNumber')
		if (parameters.level) illegalParameters.push('level')
		if (parameters.error) illegalParameters.push('error')
		if (parameters.message) illegalParameters.push('message')
		if (parameters.source) illegalParameters.push('source')
		if (illegalParameters.length > 0)
			console.warn(`Argument 'parameters' contains illegal parameters: ${illegalParameters.join(', ')}. They will be ignored.`)
		return validParameters
	}

	/**
	 * Logger creates a simple logger which can be used either directly or as a base to compose custom Loggers.
	 * Requires only source and parameters. Check examples for more details.
	 * @param source {function|string} - Usually source type or source file
	 * @param parameters {object} - Anything that should be logged along with the log messages.
	 * @return {Readonly<{warn: (function(...[*]): {[p: string]: *}), debug: (function(...[*]): {[p: string]: *}), log: (function(*=, ...[*]=): {[p: string]: *}), onMessage: *, error: (function(...[*]): {[p: string]: *}), info: (function(...[*]): {[p: string]: *}), fatal: (function(...[*]): {[p: string]: *})}>}
	 * @constructor
	 * @example
	 * import { Logging } from 'pathToLogging/logging.js'
	 *
	 * const log = Log()
	 * export const Logger = log.Logger
	 *
	 * log.start()
	 *
	 * @example
	 * // logger.js
	 * import { Log } from 'pathToLogging/logging.js'
	 *
	 * const log = Log()
	 * const Logger = (source, category, ...tags) => log.Logger(source, {category, tags})
	 * log.start()
	 * export { Logger }
	 *
	 * // shopping-cart.js
	 * import {Logger} from 'logger.js'
	 * const logger = Logger('ShoppingCart', 'business', 'cart', 'order', 'payments')
	 * // ...
	 * logger.warn.withError(fetchReviewsError, 'Item reviews could not be fetched')
	 *
	 */
	function Logger(source, parameters) {
		if (source === undefined || source === null)
			throw new ReferenceError('The \'source\' argument is not defined')
		const sourceName = (typeof source === 'function') ? source.name : source.toString()
		const [onMessage, publishMessage] = Publisher()
		const logMessage = (level, error, message) => {
			const logNumber = nextLogNumber()
			const date = new Date()
			const customLogNumber = config.logNumberGenerator(logNumber, date)
			const validParameters = validateParameters(parameters)
			const entry = {
				date,
				logNumber: customLogNumber,
				level,
				error,
				message,
				source: sourceName,
				...validParameters
			}
			handleMessage(entry)
			publishMessage(entry)
			return entry
		}

		const log = (level, ...message) => logMessage(level, undefined, message)
		log.withError = (level, error, ...message) => logMessage(level, error, message)

		const debug = (...message) => log(LogLevel.DEBUG, ...message)
		debug.withError = (error, ...message) => log.withError(LogLevel.DEBUG, error, ...message)

		const info = (...message) => log(LogLevel.INFO, ...message)
		info.withError = (error, ...message) => log.withError(LogLevel.INFO, error, ...message)

		const warn = (...message) => log(LogLevel.WARN, ...message)
		warn.withError = (error, ...message) => log.withError(LogLevel.WARN, error, ...message)

		const error = (...message) => log(LogLevel.ERROR, ...message)
		error.withError = (error, ...message) => log.withError(LogLevel.ERROR, error, ...message)

		const fatal = (...message) => log(LogLevel.FATAL, ...message)
		fatal.withError = (error, ...message) => log.withError(LogLevel.FATAL, error, ...message)

		return  Object.freeze({ log, debug, info, warn, error, fatal, onMessage })
	}

	return {
		start,
		stop,
		Logger,
		get configuration () { return config },
		get logNumber() { return logNumber },
		get customLogNumber() { return customLogNumber }
	}
}